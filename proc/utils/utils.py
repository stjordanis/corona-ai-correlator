# -*- coding: utf-8 -*-
"""
DRUGS/Corona-AI Project module: utils.py
    
Collection of functions and routines for other modules to load, map and pre-process data.    

Created on Wed Dec  5 15:47:37 2018

--------------------------------------------------------------------------
Lead Developer: Dr. Ivan Laponogov (mailto:i.laponogov@imperial.ac.uk) 

Chief project investigator: Dr. Kirill Veselkov (mailto:kirill.veselkov04@imperial.ac.uk)

License: 

The MIT License (MIT)

Copyright 2019 Imperial College London

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

"""
import numpy as np;
import os;
import sys;
import hashlib;
import configparser;
import gzip;
import psutil;

#Loading local modules
from proc.utils.printlog import start_log, stop_log, printlog;  #print and log printed messages in one go

    
def load_global_settings(settings_file):
    global_settings = configparser.ConfigParser();
    global_settings.read(settings_file);
        
    for subpath in ['Compounds', 'Disease',
                    'Interactome_Path',
                    'Results_Path',
                    'Scratch_Path',
                    ]:
        if not os.path.isabs(global_settings['Default Paths'][subpath]):
            global_settings['Default Paths'][subpath] = os.path.realpath(os.path.expanduser(os.path.join(os.path.dirname(settings_file), global_settings['Default Paths'][subpath])));
            
    return global_settings
    
    
def memory_report():
    """
    Prints current memory usage statistics

    Returns
    -------
    None.

    """
    try:
        info = str(psutil.Process().memory_info());
        info = info.split('(')[1].split(')')[0].split(', ');
        for s in info:
            s = s.split('=');
            printlog('%s: %s Mb'%(s[0], int(s[1])//1024//1024))
    except:
        pass
    
    
def cleanup_string(value):
    """
    Removes spaces and quotes from string values

    Parameters
    ----------
    value : str
        Input string to be cleaned.

    Returns
    -------
    Cleaned sting.

    """
    
    value = value.strip();
    if len(value) > 1:
        if value[0] == '"' and value[-1] == '"':
            return value[1:-1];
        elif value[0] == "'" and value[-1] == "'":
            return value[1:-1];
        else:
            return value

def get_files_list(path = os.getcwd(), recursive = True, extensions = set(['.*'])):
    """
    Looks for files in the folder and returns them as a list of full names.

    Parameters
    ----------
    path : str, optional
        Path to look for files in. The default is current working directory.
    
    recursive : bool, optional
        If True - look for files in subfolders as well. The default is True.
        
    extensions : set, optional
        Set of allowed extensions. If file extension is not in the list and there is
        no '.*' in the list - the file is ignored. The default is set(['.*']), 
        meaning all files will be included even if you provide other extensions.

    Returns
    -------
    list
        List of found files with full paths.

    """
    #Get absolute path
    path = os.path.realpath(os.path.expanduser(path));
    
    result = [];
    if recursive:
        result = [os.path.join(dp, f) for dp, dn, filenames in os.walk(path) for f in filenames if (os.path.splitext(f)[1] in extensions) or ('.*' in extensions)]
    else:
        result = [os.path.join(path, f) for f in os.listdir(path) if (os.path.isfile(os.path.join(path, f))) and ((os.path.splitext(f)[1] in extensions) or ('.*' in extensions))];
    
    return result;
    
    
