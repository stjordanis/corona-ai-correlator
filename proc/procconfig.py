# -*- coding: utf-8 -*-
"""

***********************************************
Command line options for different modules
***********************************************

 
"""

#===========================Import section=================================

#Importing standard and external modules
import os
import sys
import tempfile

#If run independently - check system endianness and add path to local modules
if __name__ == "__main__":
    if sys.byteorder!='little':
        print('Only little endian machines currently supported! bye bye ....');
        quit();
    module_path = os.path.abspath('%s/..'%os.path.dirname(os.path.realpath(__file__)));
    sys.path.insert(0, module_path);

#Import local/internal modules
from proc.utils.cmdline import Option, Value, AllValues, PathExists, GreaterOrEqual;

#==========================================================================

#Settings for the command line interfaces for different modules
                    
#Common logfile options - set for all modules                    
logfile_options = [
                   Option('--logfile', 
                          help = 'The file name to which to write the log.', 
                          values = ['', None], 
                          type = str, 
                          level = 0),

                   Option('--overwrite_logfile', 
                          help = 'Overwrite existing log or not.', 
                          values = ['no', 'yes'], 
                          type = str, 
                          level = 0),

                   Option('--verbose', 
                          help = 'Overwrite existing log or not.', 
                          values = [0, 1, 2, None], 
                          type = int, 
                          level = 0), #Level of verbosity
                   ];



"""

These options are the template for a typical module. For details on how to 
define all parameters for them for the command line parser - see corresponding 
module documentation for cmdline.py in utils.

In brief, you need to define a list of instances of class Option. Each instance 
is one defined command line option. 

Instances are initialized as follows with defaults indicated:

Option(option, help = '', values = [None], is_list = False, type = None, conditions = [], targets = [], optional = True, level = 0)

with parameters:

1) option: defines the name of the option and how it is presented at the command 
line. Compulsory. Type: str. Option names follow normal python variable name
requirements, i.e. alpha-numeric and underscore allowed. If prefixed with '-' -
it is a named argument and can be positioned anywhere in the command line parameters,
while not prefixed options are positional and should be supplied in the order defined 
in the list. Single '-' in front of single letter indicates a short version of the option, 
'--' indicates full option name, both can be supplied if separated by coma. 
Full version is compulsory, short version is optional.

Example:
    '--h5readpath' - option with full name 'h5readpath'. This is the only form 
    accepted. In the command line it should be supplied as 
    --h5readpath "/some/path".
    
    '-r,--h5readpath' - option 'h5readpath' is defined by both short and full 
    versions. In the command line it can be supplied as either
    -r "/some/path"
    or
    --h5readpath "/some/path"
    
    'dbfilename' - positional argument option, it will be named dbfilename 
    internally, but supplied in the command line according to its sequence order
    in the list of positional argument options.
    
2) help = '': help string describing option. Used to construct automatic help

3) values = [None]: List of allowed values. First value in the list is used as
    default if option is not specified in the parsed command line. If None is
    present in the list - any value is accepted for the option - otherwise only
    the values from this supplied list are accepted. If values is set to None,
    the option is considered a flag switch and does not expect a value to be 
    supplied after it in the command line. In this case its value is set to True
    if this option was supplied in the command line and False otherwise.

4) is_list = False: expect multiple values supplied as coma-separated list in
    the command line. Otherwise only one value is taken.

5) type = None: if not None - the input values are checked and converted to the 
    type specified. E.g. type = str will convert the value to string, type = int 
    to integer value etc.

6) conditions = []: Set of conditions for the entered values to be checked against
    during parsing.

7) targets = []: If provided - overrides the default hierarchial parameters 
    structure to which the output option value is sent. Useful if different 
    selected values of the option would change the default values of other
    global options. This is more advanced topic and is not discussed in detail 
    here.

8) optional = True: Whether this option can be omitted in the command line or
    it should always be provided.

9) level = 0: Level of this option for HTML parameter tree generation. level 0
    options are shown to all users, level 1 - only to advanced ones. Not used 
    in basic command line argument parsing     


When the command line arguments are parsed by OptionsHolder instance method
parse_command_line_args(), they are stored in the parameters variable of this 
instance. Parameters is a dictionary with options as keys and their values 
assigned as values. 

Example: 

for command line options [Option('--h5readpath'), Option('dbfilename')]

and command line: python <yourmodule>.py --h5readpath "/subpath1" "/db1.fname"
you will have 
parameters = {"h5readpath":"/subpath1", "dbfilename":"/db1.fname"};
in your instance of OptionsHolder.


You may have a hierarchial form of options where subsequent options are defined
depending upon the parent option value provided. This is a more complex case
and is described in detail in the documentation for cmdline.py. 
In brief, if you have the option which value selection defines which other options
are becoming relevant - you can supply its possible values in the values = []
list using Value instances. In this case only Value instances will be allowed in 
values list and no None is allowed there.

Each Value(value, help='', parameters=[]) instance is initiated with:
value: option value 
help: help line describing this particular pre-selectable value for the parent 
    option
parameters: list of Option instances which are dependent upon the selection
    of this particular value. 
    
By default sub-options values are placed in parameters in
"parentoption_parentoptionvalue", but if their targets list is populated,
they are redirected into the structure provided by the string in the targets list.
This way you can change the default values for Options which depend upon the 
pre-selection of other Options values. This is recursive procedure, so in theory
any level of complexion is achieveable. 
See details in the cmdline.py documentation.
    
"""

Default_options = [
                    Option('Settings_INI_file', 
                           help = 'The name and the path to the settings.ini '\
                                  'file to control workflow. ', 
                           values = ['', None], 
                           type = str,
                           optional = False,
                           level = 1),

                    ] + \
                  logfile_options;

                   