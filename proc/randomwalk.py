# -*- coding: utf-8 -*-
"""
Created on Fri May 29 13:39:35 2020

@author: Ivan
"""

import os
import sys


import numpy as np
import scipy.sparse as sp;

if __name__ == "__main__": 
    if sys.byteorder!='little':
        print('Only little endian machines currently supported! bye bye ....');
        quit();
    module_path = os.path.abspath('%s/../..'%os.path.dirname(os.path.realpath(__file__)));
    sys.path.insert(0, module_path);
    

#Timing functions for standard stats output
from proc.utils.timing import tic, toc;
from proc.utils.printlog import printlog, start_log, stop_log;



def randomwalk_sparse_matrix(vector, source_indices, target_indices, weights = None, c = 0.01, max_cycles = 100, normalize_input = True, normalize_output = True, normalize_connections = True, eps = 1.0e-6):
    """
    Random Walk based propagation algorithm. 

    Parameters
    ----------
    vector : 2D or 1D float array or convertable: (N_samples, N_nodes) or (N_nodes)
        Starting values for nodes.
    
    source_indices : 1D int array or convertable
        Indices of source nodes for edges.
    
    target_indices : 1D int array or convertable
        Indices of target nodes for edges. Must be of the same size as source_indices.
    
    weights : 1D float array or convertable, if None - assumed to be 1.0 for all edges
        Weight for each edge. Must match the sizes of source_indices and target_indices
    
    c : float (0.0-1.0) or 1D array of floats, default - 0.01
        Restart rate constant for random walk. 0.0 means no returns to the start. 1.0 means no propagation. The smaller c - the further signal propagates.
    
    max_cycles : int, default - 100
        Limit maximum number of cycles
    
    normalize_input : bool
        Normalize input vector to a sum of 1.0. Default: True
        
    normalize_output : bool
        Normalize output vector to a sum of 1.0. Default: True
        
    normalize_connections : bool
        Normalize weights of the outgoing connections to a sum of 1.0 to reflect transition probability. Default: True
        
    eps : float 
        Tolerance for the convergence of the algorithm, default: 1.0e-6.



    Returns
    -------
    tuple of 1D numpy float64 array containing propagated vector(s) values,
    number of cycles passed and last eps between cycles. eps is either a float or
    1D array of floats depending upon the input vector

    """
    
    #Assure correct type and convert if needed
    p_start = np.array(vector, dtype = np.float64);
    
    if len(p_start.shape) != 1 and len(p_start.shape) != 2:
        raise ValueError('Input vector should be 1D (N_nodes) or 2D (N_samples, N_nodes) array!');
    
    if len(p_start.shape) == 1:
        p_start = p_start.reshape((1, -1));
        
    source_indices = np.array(source_indices, dtype = np.uint32);
    target_indices = np.array(target_indices, dtype = np.uint32);
    
    if weights is None:
        weights = np.ones((len(source_indices), ), dtype = np.float64);
    else:
        weights = np.array(weights, dtype = np.float64);
    
    #Check that array sizes match
    assert(len(source_indices) == len(target_indices))
    assert(len(source_indices) == len(weights))
    
    #Check indexing is within range
    assert(np.max(source_indices) < p_start.shape[1]);
    assert(np.max(target_indices) < p_start.shape[1]);

    #Normalize the input vector to a sum of 1.0    
    if normalize_input:
        p_start = np.divide(p_start, np.sum(p_start, axis = 1).reshape(-1, 1));

    #If c is 1D array (1 c value per input sample) - reshape 
    if isinstance(c, np.ndarray):
        c = c.reshape(-1, 1);

    #p_prev will hold vector values from a previous cycle
    p_prev = np.zeros(p_start.shape, dtype = np.float64);
    
    #p_current will hold vector values from a current cycle    
    p_current = np.copy(p_start);
    
    #Calculate discrepancy eps between vectors from two cycles
    eval_epses = np.max(np.abs(np.subtract(p_prev, p_current)), axis = 1);
    eval_eps = np.max(eval_epses);
    
    n_cycle = 0;
    
    #Adjucency sparse matrix is generated from input vectors as coo_matrix for fast creation
    A = sp.coo_matrix((weights, (source_indices, target_indices)), shape = (p_start.shape[1], p_start.shape[1]), dtype = np.float64);
    
    #Convert adjucency matrix to compressed sparse column format for fast operations. It is a bit faster than compressed sparse row one in our tests, but can be interchanged.
    A = sp.csc_matrix(A);
    
    #Normalize matrix outgoing weights, so that they sum to 1.0 to reflect probability of transition.
    if normalize_connections:
        outgoing_weights_sum = A.sum(axis = 1);
        #Correct for zero sums where there are no outgoing connections for the node
        outgoing_weights_sum[outgoing_weights_sum == 0.0] = 1.0;
        #Normalize the matrix via dot product with a diagonal matrix of scaling coefficients
        outgoing_weights_sum = 1.0 / outgoing_weights_sum;
        A = sp.diags([outgoing_weights_sum.flatten()], offsets = [0], shape = A.shape).dot(A);
    
    #Normally the cycle would use new_vector = dot(current_vector, A) * (1.0 - c) + c * starting_vector 
    #operation to calculate transition probabilities,
    #but since numpy does not work well with sparse matrices, it has to be a work-around:
    #dot(vector, A) = A.T.dot(vector.T).T
    #So we will pre-transpose the matrix
    A = A.transpose();
    
    #Continue cycling unless the error is less than tolerance or the number of cycles is exceeded
    while ((eval_eps > eps) and (n_cycle < max_cycles)):
        print('Cycle: %s'%n_cycle);
        #copy current vector into previous one without memory re-allocation
        p_prev[:] = p_current[:];
        #calculate new current vector
        p_current = np.add(np.multiply(A.dot(p_prev.T).T, np.subtract(1.0, c)), np.multiply(c, p_start));        
        #Re-calculate error
        eval_epses = np.max(np.abs(np.subtract(p_prev, p_current)), axis = 1);
        eval_eps = np.max(eval_epses);
        #advance cycle counter
        n_cycle += 1;
        

    if n_cycle == max_cycles:
        printlog('Warning! Random walk propagation did not converge!');
    
    #Normalize final vector to a sum of 1.0
    if normalize_output:
        p_current = np.divide(p_current, np.sum(p_current, axis = 1).reshape(-1, 1));
    
    if p_current.shape[0] == 1:
        return p_current.flatten(), n_cycle, eval_eps;
    else:
        return p_current, n_cycle, eval_epses;




