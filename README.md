# README #
This package serves as a demo/validator of the methodology used in manuscript __Network machine learning maps phytochemically-rich “Hyperfoods” to fight COVID-19 (2020)__.
With it you can reproduce the results of the paper one single set of parameters at a time. The package comes with all the necessary data including
the genes, gene-gene connections, disease-affected genes and compounds of interest with mapped gene targets. While the focus of this package is SARS-CoV-2
infection, it can easily be repurposed for other diseases with known host/human gene targets and established "positive" and "negative" class drugs which
are used/not used against this disease respectively. 


## Setup and Test ##
-----

You will need Python 3, 64bit to run this code. Also you will need to have at least 16Gb of free RAM (32 Gb recommended).

Please, install [Anaconda Python Individual](https://www.anaconda.com/products/individual) or even 
better [Miniconda](https://docs.conda.io/en/latest/miniconda.html) to focus only on the packages that you need.  

Create environment for __CoronaAI__ within Anaconda Python by running the following commands in the terminal (macOS/Linux) or Anaconda Prompt (Windows): 

#### Windows: ####

```
conda create -n CoronaAI python=3.6
activate CoronaAI
conda install -n CoronaAI numpy scipy pandas scikit-learn psutil
```

#### macOS/Linux: ####
```
[sudo] conda create -n CoronaAI python=3.6
[sudo] source activate CoronaAI
[sudo] conda install -n CoronaAI numpy scipy pandas scikit-learn psutil
```
**Note** optional `sudo` command for Linux/macOS - you may or may not need to use it depending where and how you installed Anaconda. If you can get it to run without `sudo` - this is a preferred option.

Download the current [__CoronaAI__ release](https://bitbucket.org/iAnalytica/corona-ai-correlator/src/master/) and unpack it in your folder. 

Navigate to your __CoronaAI__ folder in your command prompt (i.e. the one containing the __process.py__ file) and run the code with the provided default __settings__:


```
cd <Where you extracted Corona-AI-correlator>
python process.py settings.ini
``` 

__process.py__ takes settings file name as a single positional command line argument. For the list of optional command line arguments run:

```
python process.py --help
``` 


__settings.ini__ contains the default (the best for "aggregated" interactome) settings. See comments inside this file for detailed description of the available parameters and accepted values.


It takes a few hours to propagate and process all the compounds on a normal PC.

If you need to try multiple settings, it is best to produce multiple uniquely named __settings_*.ini__ files with individual combinations of parameter values and then
use distributed computing to process them in parallel.

Once the processing is completed, __Results__ subfolder will have both class predictions 
for the compounds (in __Compounds.csv__) and the processing statistics 
(__classification_stats.csv__ for individual folds/splits, __mean_classification_stats.csv__ for averaged values) in the subfolder named the same way as your input settings file. 
This allows one to have one shared __Results__ folder for multiple parallel instances of this code processing different settings.


## Acknowledgments ##

Any papers describing data analytics using this package, or whose results were significantly aided by the use of this package (except when the use was internal to a larger program), should include an acknowledgment and citation to the following manuscript(s):

1. Laponogov and Gonzalez et al. Network machine learning maps phytochemically-1 rich “Hyperfoods” to fight COVID-19 (2020) (submitted)

If the package is used prior to its open source release and publication, please contact [kirill.veselkov04@imperial.ac.uk](mailto:kirill.veselkov04@imperial.ac.uk) to ensure that all contributors are fairly acknowledged.


## Legality and Disclaimers ##

Contents of this container are provided __AS IS__ and 
authors accept no responsibility for whatever they are going to be used for or any consequences.
Use them at your own risk. 


Authors
-------
Lead Developers: 
[Dr. Ivan Laponogov](mailto:i.laponogov@imperial.ac.uk) 

Chief project investigator: 
[Dr. Kirill Veselkov](mailto:kirill.veselkov04@imperial.ac.uk)

Copyright, Imperial College London

## Issues ##

Please report any bugs or requests that you have using the Bitbucket issue tracker!

## License: ##
The code is released under permissive MIT license. See LICENSE document in the root folder of this package.

## Funding: ##

Vodafone Foundation CORONA-AI/DRUGS Projects, ERC Hyperfoods project. 

